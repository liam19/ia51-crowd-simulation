package fr.utbm.ia51.crowdsim.gui

import javafx.application.Application
import javafx.stage.Stage
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.Group
import javafx.scene.image.ImageView
import javafx.scene.image.Image
import javafx.scene.control.TextField
import javafx.^event.EventHandler
import javafx.^event.ActionEvent
import javafx.scene.text.Text
import javafx.scene.text.Font
import javafx.scene.text.FontPosture
import javafx.scene.text.FontWeight
import javafx.scene.paint.Color
import io.sarl.bootstrap.SREBootstrap
import io.sarl.bootstrap.SRE
import fr.utbm.ia51.crowdsim.environment.^agent.Environment
import java.io.File
import javafx.stage.FileChooser

/**
 * First UI presented to the user.
 * Allows the user to enter some parameters for the simulation.
 */
class HomeUI extends Application {
	
	public static var singleton : HomeUI

	var tfRiotersDensity : TextField = new TextField
	var tfOfficersDensity : TextField = new TextField
	var tfBreakerProbability : TextField = new TextField
	val height = 875
	val width = 1300
	val fileChooser : FileChooser = new FileChooser
	var config_file : File = null
	var tfImportCSVName : TextField
	var cl = class.classLoader
	
	def start(primaryStage : Stage) {
		singleton = this
		primaryStage.setTitle("Protest simulation - Yellow vests edition")
		
		
		tfRiotersDensity.setPromptText("Number of rioters")
		tfRiotersDensity.setLayoutX(500)
		tfRiotersDensity.setLayoutY(450)
		tfRiotersDensity.setText("100")

		tfOfficersDensity.setPromptText("Number of officers")
		tfOfficersDensity.setLayoutX(500)
		tfOfficersDensity.setLayoutY(370)
		tfOfficersDensity.setText("5")

		tfBreakerProbability.setPromptText("Probability of breakers (%)")
		tfBreakerProbability.setLayoutX(500)
		tfBreakerProbability.setLayoutY(330)
		tfBreakerProbability.setText("10")
		
		var root : Group = new Group
		var imgvw : ImageView = new ImageView(
			new Image(cl.getResource("images/giletjaune.jpg").toString))
		
		var txtRiotersDensity : Text = new Text("Number of rioters :")
		txtRiotersDensity.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12))
		txtRiotersDensity.setFill(Color.WHITE)
		txtRiotersDensity.setLayoutX(350)
		txtRiotersDensity.setLayoutY(468)
		
		var txtOfficerDensity : Text = new Text("Number of officers :")
		txtOfficerDensity.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12))
		txtOfficerDensity.setFill(Color.WHITE)
		txtOfficerDensity.setLayoutX(350)
		txtOfficerDensity.setLayoutY(382)

		var txtBreakerProbability : Text = new Text("Probability of breakers (%) :")
		txtBreakerProbability.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12))
		txtBreakerProbability.setFill(Color.WHITE)
		txtBreakerProbability.setLayoutX(290)
		txtBreakerProbability.setLayoutY(340)
		
		var txtImportCSV : Text = new Text("Import an existing config :")
		txtImportCSV.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12))
		txtImportCSV.setFill(Color.WHITE)
		txtImportCSV.setLayoutX(312)
		txtImportCSV.setLayoutY(425)
		
		tfImportCSVName = new TextField
		tfImportCSVName.setEditable(false)
		tfImportCSVName.setLayoutX(500)
		tfImportCSVName.setLayoutY(410)
		
		
		var btnImport : Button = new Button
		btnImport.setText("Open")
		btnImport.setLayoutX(650)
		btnImport.setLayoutY(410)
		var evntImport : EventHandler<ActionEvent> = new EventHandler<ActionEvent>() {
			def handle(e : ActionEvent) {
				config_file = fileChooser.showOpenDialog(primaryStage)
				if (config_file !== null) {
					tfImportCSVName.setText(config_file.getName)
				}
			}
		}
		btnImport.setOnAction(evntImport)

		var btn : Button = new Button
		btn.setText("Launch simulation")
		btn.setLayoutX(650)
		btn.setLayoutY(450)
		var evnt : EventHandler<ActionEvent> = new EventHandler<ActionEvent>() {
			def handle(e : ActionEvent) {
				try {
					var riotersNumber : int = 1
					var breakerProbability : int = 1
					var officersNumber : int = 1
					if (config_file === null) {
						riotersNumber = Integer.parseInt(tfRiotersDensity.getText)
						breakerProbability = Integer.parseInt(tfBreakerProbability.getText)
						officersNumber = Integer.parseInt(tfOfficersDensity.getText)
					}
					
					if (riotersNumber >= 0 && riotersNumber <= 1000) {
						primaryStage.close
						var secondStage : Stage = new SimulationUI(width, height)
						secondStage.show
						var bootstrap : SREBootstrap = SRE.getBootstrap
						
						bootstrap.startAgent(typeof(Environment), riotersNumber, officersNumber, breakerProbability, width, height, config_file, secondStage)
					}
					else {
						new ErrorAlert("Number of rioters must be between 0 and 1000.")
					}
				} catch (err : NumberFormatException) {
					new ErrorAlert("Bad format.")
				}
			}
		}
		btn.setOnAction(evnt)
		
		
		root.getChildren.add(imgvw)
		root.getChildren.add(btn)
		root.getChildren.add(tfRiotersDensity)
		root.getChildren.add(txtRiotersDensity)
		root.getChildren.add(txtImportCSV)
		root.getChildren.add(btnImport)
		root.getChildren.add(tfImportCSVName)
		root.getChildren.add(txtOfficerDensity)
		root.getChildren.add(tfOfficersDensity)
		root.getChildren.add(tfBreakerProbability)
		root.getChildren.add(txtBreakerProbability)
		
		primaryStage.setScene(new Scene(root, 854, 569))
		primaryStage.show()
	}	
}
