# IA51 Crowd Simulation

## Introduction

Crowd Simulation project using SARL to implement a Multi-Agent System.
Within the framework of UV IA51, University of Technology of Belfort-Montbéliard.

## Installation & Usage

To execute the jar artifact available in the repository `artifact`, open a Shell or Cmd Console and launch the program with the following line : `java -jar .\crowdsim.jar -o fr.utbm.ia51.crowdsim.boot.Boot`.

If you need to generate a new .jar from the source code, update the file `MANIFEST.MF` available in the newly generated jar file with the following line : `Main-Class: io.janusproject.Boot`.